jQuery(function ($) {
    "use strict";

    //===== ENABLE FORM FOR USER INPUT =================================================================================
    $('.disable-cover').hide();

    //===== SELECTS OPTIONS FILL =======================================================================================

    // Fill select options form list.
    function optionsFill($parentObj, optList) {
        for (var value in optList) {
            $parentObj.append(jQuery("<option>").val(value).text(optList[value]));
        }
    }

    // Fill grouped select options from separated groups lists.
    function groupedOptionsFill($parentObj, groupName, optList) {
        $parentObj.append($("<optgroup>").attr("label", groupName));
        optionsFill($parentObj.find("optgroup[label='"+groupName+"']"), optList);
    }

    var years_list = {};
    for (var year = 1910; year <= new Date().getFullYear(); year++) {
        years_list[year] = year;
    }
    optionsFill($(".form-row__field_year"), years_list);

    var months_list = {
        "1": "Januari (01)",
        "2": "Februari (02)",
        "3": "Mars (03)",
        "4": "April (04)",
        "5": "Maj (05)",
        "6": "Juni (06)",
        "7": "Juli (07)",
        "8": "Augusti (08)",
        "9": "September (09)",
        "10": "Oktober (10)",
        "11": "November (11)",
        "12": "December (12)"
    };
    optionsFill($(".form-row__field_month"), months_list);

    var days_list = {};
    for (var day = 1; day <= 31; day++) {
        days_list[day] = (day<10) ? ("0"+day) : day;
    }
    optionsFill($(".form-row__field_day"), days_list);

    var municip_group1_list = {
        "karlshamn": "Karlshamns kommun",
        "karlskrona": "Karlskrona kommun",
        "olofstrom": "Olofströms kommun",
        "ronneby": "Ronneby kommun",
        "solvesborg": "Sölvesborgs kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Blekinge län", municip_group1_list);
    var municip_group2_list = {
        "avesta": "Avesta kommun",
        "borlange": "Borlänge kommun",
        "falu": "Falu kommun",
        "gagnef": "Gagnefs kommun",
        "hedemora": "Hedemora kommun",
        "leksand": "Leksands kommun",
        "ludvika": "Ludvika kommun",
        "malung": "Malungs kommun",
        "mora": "Mora kommun",
        "orsa": "Orsa kommun",
        "rattvik": "Rättviks kommun",
        "smedjebacken": "Smedjebackens kommun",
        "sater": "Säters kommun",
        "vansbro": "Vansbro kommun",
        "alvdalen": "Älvdalens kommun",
    };
    groupedOptionsFill($(".form-row__field_municip"), "Dalarnas län", municip_group2_list);
    var municip_group3_list = {
        "gotland": "Gotlands kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Gotlands län", municip_group3_list);
    var municip_group4_list = {
        "bollnas": "Bollnäs kommun",
        "gavle": "Gävle kommun",
        "hofors": "Hofors kommun",
        "hudiksvall": "Hudiksvalls kommun",
        "ljusdal": "Ljusdals kommun",
        "nordanstig": "Nordanstigs kommun",
        "ockelbo": "Ockelbo kommun",
        "ovanaker": "Ovanåkers kommun",
        "sandviken": "Sandvikens kommun",
        "soderhamn": "Söderhamns kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Gävleborgs län", municip_group4_list);
    var municip_group5_list = {
        "falkenberg": "Falkenbergs kommun",
        "halmstad": "Halmstads kommun",
        "hylte": "Hylte kommun",
        "kungsbacka": "Kungsbacka kommun",
        "laholm": "Laholms kommun",
        "varberg": "Varbergs kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Hallands län", municip_group5_list);
    var municip_group6_list = {
        "berg": "Bergs kommun",
        "bracke": "Bräcke kommun",
        "harjedalen": "Härjedalens kommun",
        "krokom": "Krokoms kommun",
        "ragunda": "Ragunda kommun",
        "stromsund": "Strömsunds kommun",
        "are": "Åre kommun",
        "ostersund": "Östersunds kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Jämtlands län", municip_group6_list);
    var municip_group7_list = {
        "aneby": "Aneby kommun",
        "eksjo": "Eksjö kommun",
        "gislaved": "Gislaveds kommun",
        "gnosjo": "Gnosjö kommun",
        "habo_kommun": "Habo Kommun",
        "jonkoping": "Jönköpings kommun",
        "mullsjo": "Mullsjö kommun",
        "nassjo": "Nässjö kommun",
        "savsjo": "Sävsjö kommun",
        "tranas": "Tranås kommun",
        "vaggeryd": "Vaggeryds kommun",
        "vetlanda": "Vetlanda kommun",
        "varnamo": "Värnamo kommun"

    };
    groupedOptionsFill($(".form-row__field_municip"), "Jönköpings län", municip_group7_list);
    var municip_group8_list = {
        "borgholm": "Borgholms kommun",
        "emmaboda": "Emmaboda kommun",
        "hultsfred": "Hultsfreds kommun",
        "hogsby": "Högsby kommun",
        "kalmar": "Kalmar kommun",
        "monsteras": "Mönsterås kommun",
        "morbylanga": "Mörbylånga kommun",
        "nybro": "Nybro kommun",
        "oskarshamn": "Oskarshamns kommun",
        "torsas": "Torsås kommun",
        "vimmerby": "Vimmerby kommun",
        "vastervik": "Västerviks kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Kalmar län", municip_group8_list);
    var municip_group9_list = {
        "alvesta": "Alvesta kommun",
        "lessebo": "Lessebo kommun",
        "ljungby": "Ljungby kommun",
        "markaryd": "Markaryds kommun",
        "tingsryd": "Tingsryds kommun",
        "uppvidinge": "Uppvidinge kommun",
        "vaxjo": "Växjö kommun",
        "almhult": "Älmhults kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Kronobergs län", municip_group9_list);
    var municip_group10_list = {
        "arjeplog": "Arjeplogs kommun",
        "arvidsjaur": "Arvidsjaurs kommun",
        "boden": "Bodens kommun",
        "gallivare": "Gällivare kommun",
        "haparanda": "Haparanda kommun",
        "jokkmokk": "Jokkmokks kommun",
        "kalix": "Kalix kommun",
        "kiruna": "Kiruna kommun",
        "lulea": "Luleå kommun",
        "pajala": "Pajala kommun",
        "pitea": "Piteå kommun",
        "alvsbyn": "Älvsbyns kommun",
        "overkalix": "Överkalix kommun",
        "overtornea": "Övertorneå kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Norbottens län", municip_group10_list);
    var municip_group11_list = {
        "bjuv": "Bjuvs kommun",
        "bromolla": "Bromölla kommun",
        "burlov": "Burlövs kommun",
        "bastad": "Båstads kommun",
        "eslov": "Eslövs kommun",
        "helsingborg": "Helsingborgs stad",
        "hassleholm": "Hässleholms kommun",
        "hoganas": "Höganäs kommun",
        "horby": "Hörby kommun",
        "hoor": "Höörs kommun",
        "klippan": "Klippans kommun",
        "kristianstad": "Kristiandstads kommun",
        "kavlinge": "Kävlinge kommun",
        "landskrona": "Landskrona kommun",
        "lomma": "Lomma kommun",
        "lund": "Lunds kommun",
        "malmo": "Malmö stad",
        "osby": "Osby kommun",
        "perstorp": "Perstorps kommun",
        "simrishamn": "Simrishamns kommun",
        "sjobo": "Sjöbo kommun",
        "skurup": "Skurups kommun",
        "staffanstorp": "Staffantorps kommun",
        "svalov": "Svalövs kommun",
        "svedala": "Svedala kommun",
        "tomelilla": "Tomelilla kommun",
        "trelleborg": "Trelleborgs kommun",
        "vellinge": "Vellinge kommun",
        "ystad": "Ystads kommun",
        "astorp": "Åstorps kommun",
        "angelholm": "Ängelholms kommun",
        "orkelljunga": "Örkelljunga kommun",
        "ostra_goinge": "Östra Göinge kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Skåne län", municip_group11_list);
    var municip_group12_list = {
        "botkyrka": "Botkyrka kommun",
        "danderyd": "Danderyds kommun",
        "ekero": "Ekerö kommun",
        "haninge": "Haninge kommun",
        "huddinge": "Huddinge kommun",
        "jarfalla": "Järfälla kommun",
        "lidingo": "Lidingö stad",
        "nacka": "Nacka kommun",
        "norrtalje": "Norrtälje kommun",
        "nykvarn": "Nykvarns kommun",
        "nynashamn": "Nynäshamns kommun",
        "salem": "Salems kommun",
        "sigtuna": "Sigtuna kommun",
        "sollentuna": "Sollentuna kommun",
        "solna": "Solna stad",
        "stockholm": "Stockholms stad",
        "sundbyberg": "Sundbybergs stad",
        "sodertalje": "Södertälje kommun",
        "tyreso": "Tyresö kommun",
        "taby": "Täby kommun",
        "upplands_vasby": "Upplands Väsby kommun",
        "upplands-bro": "Upplands-Bro kommun",
        "vallentuna": "Vallentuna kommun",
        "vaxholm": "Vaxholms stad",
        "varmdo": "Värmdö kommun",
        "osteraker": "Österåkers kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Stockholms län", municip_group12_list);
    var municip_group13_list = {
        "eskilstuna": "Eskilstuna kommun",
        "flen": "Flens kommun",
        "gnesta": "Gnesta kommun",
        "katrineholm": "Katrineholms kommun",
        "nykoping": "Nyköpings kommun",
        "oxelosund": "Oxelösunds kommun",
        "strangnas": "Strängnäs kommun",
        "trosa": "Trosa kommun",
        "vingaker": "Vingåkers kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Södermanlands län", municip_group13_list);
    var municip_group14_list = {
        "enkoping": "Enköpings kommun",
        "habo": "Håbo kommun",
        "knivsta": "Knivsta kommun",
        "tierp": "Tierps kommun",
        "uppsala": "Uppsala kommun",
        "alvkarleby": "Älvkarleby kommun",
        "osthammar": "Östhammars kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Uppsala län", municip_group14_list);
    var municip_group15_list = {
        "arvika": "Arvika kommun",
        "eda": "Eda kommun",
        "filipstad": "Filipstads kommun",
        "forshaga": "Forshaga kommun",
        "grums": "Grums kommun",
        "hagfors": "Hagfors kommun",
        "hammaro": "Hammarö kommun",
        "karlstad": "Karlstads kommun",
        "kil": "Kils kommun",
        "kristinehamn": "Kristinehamns kommun",
        "munkfors": "Munkfors kommun",
        "storfors": "Storfors kommun",
        "sunne": "Sunne kommun",
        "saffle": "Säffle kommun",
        "torsby": "Torsby kommun",
        "arjang": "Årjängs kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Värmlands län", municip_group15_list);
    var municip_group16_list = {
        "bjurholm": "Bjurholms kommun",
        "dorotea": "Dorotea kommun",
        "lycksele": "Lycksele kommun",
        "mala": "Malå kommun",
        "nordmaling": "Nordmalings kommun",
        "norsjo": "Norsjö kommun",
        "robertsfors": "Robertsfors kommun",
        "skelleftea": "Skellefteå kommun",
        "sorsele": "Sorsele kommun",
        "storumans": "Storumans kommun",
        "umea": "Umeå kommun",
        "vilhelmina": "Vilhelmina kommun",
        "vindeln": "Vindelns kommun",
        "vannas": "Vännäs kommun",
        "asele": "Åsele kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Västerbottens län", municip_group16_list);
    var municip_group17_list = {
        "harnosand": "Härnösands kommun",
        "kramfors": "Kramfors kommun",
        "solleftea": "Sollefteå kommun",
        "sundsvall": "Sundsvalls kommun",
        "timra": "Timrå kommun",
        "ange": "Ånge kommun",
        "ornskoldsvik": "Örnsköldsviks kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Västernorrlands län", municip_group17_list);
    var municip_group18_list = {
        "arboga": "Arboga kommun",
        "fagersta": "Fagersta kommun",
        "hallstahammar": "Hallstahammars kommun",
        "heby": "Heby kommun",
        "kungsor": "Kungsörs kommun",
        "koping": "Köpings kommun",
        "norberg": "Norbergs kommun",
        "sala": "Sala kommun",
        "skinnskatteberg": "Skinnskattebergs kommun",
        "surahammar": "Surahammars kommun",
        "vasteras": "Västerås stad"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Västmanlands län", municip_group18_list);
    var municip_group19_list = {
        "ale": "Ale kommun",
        "alingsas": "Alingsås kommun",
        "bengtsfors": "Bengtsfors kommun",
        "bollebygd": "Bollebygds kommun",
        "boras": "Borås kommun",
        "dals-ed": "Dals-Eds kommun",
        "essunga": "Essunga kommun",
        "falkoping": "Falköpings kommun",
        "fargelanda": "Färgelanda kommun",
        "grastorp": "Grästorps kommun",
        "gullspang": "Gullspångs kommun",
        "goteborg": "Göteborgs stad",
        "gotene": "Götene kommun",
        "herrljunga": "Herrljunga kommun",
        "hjo": "Hjo kommun",
        "harryda": "Härryda kommun",
        "karlsborg": "Karlsborgs kommun",
        "kungalv": "Kungälvs kommun",
        "lerum": "Lerums kommun",
        "lidkoping": "Lidköpings kommun",
        "lilla_edet": "Lilla Edets kommun",
        "lysekil": "Lysekils kommun",
        "mariestad": "Mariestads kommun",
        "mark": "Marks kommun",
        "mellerud": "Melleruds kommun",
        "munkedal": "Munkedals kommun",
        "molndal": "Mölndals kommun",
        "orust": "Orust kommun",
        "partille": "Partille kommun",
        "skara": "Skara kommun",
        "skovde": "Skövde kommun",
        "sotenas": "Sotenäs kommun",
        "stenungsund": "Stenungsunds kommun",
        "stromstad": "Strömstads kommun",
        "svenljunga": "Svenljunga kommun",
        "tanum": "Tanums kommun",
        "tibro": "Tibro kommun",
        "tidaholm": "Tidaholms kommun",
        "tjorn": "Tjörns kommun",
        "tranemo": "Tranemo kommun",
        "trollhattan": "Trollhättans kommun",
        "toreboda": "Töreboda kommun",
        "uddevalla": "Uddevalla kommun",
        "ulricehamn": "Ulricehamns kommun",
        "vara": "Vara kommun",
        "vargarda": "Vårgårda kommun",
        "vanersborg": "Vänersborgs kommun",
        "amal": "Åmåls kommun",
        "ockero": "Öckerö kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Västra Götalands län", municip_group19_list);
    var municip_group20_list = {
        "askersund": "Askersunds kommun",
        "degerfors": "Degerfors kommun",
        "hallsberg": "Hallsbergs kommun",
        "hallefors": "Hällefors kommun",
        "karlskoga": "Karlskoga kommun",
        "kumla": "Kumla kommun",
        "laxa": "Laxå kommun",
        "lekeberg": "Lekebergs kommun",
        "lindesberg": "Lindesbergs kommun",
        "ljusnarsberg": "Ljusnarsbergs kommun",
        "nora": "Nora kommun",
        "orebro": "Örebro kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Örebro län", municip_group20_list);
    var municip_group21_list = {
        "boxholm": "Boxholms kommun",
        "finspang": "Finspångs kommun",
        "kinda": "Kinda kommun",
        "linkoping": "Linköpings kommun",
        "mjolby": "Mjölby kommun",
        "motala": "Motala kommun",
        "norrkoping": "Norrköpings kommun",
        "soderkoping": "Söderköpings kommun",
        "vadstena": "Vadstena kommun",
        "valdemarsvik": "Valdemarsviks kommun",
        "ydre": "Ydre kommun",
        "atvidaberg": "Åtvidabergs kommun",
        "odeshog": "Ödeshögs kommun"
    };
    groupedOptionsFill($(".form-row__field_municip"), "Östergötlands län", municip_group21_list);

    var countries_list = {
        "SWE": "Sverige",
        "AFG": "Afghanistan",
        "ALA": "Åland",
        "ALB": "Albanien",
        "ALG": "Algeriet",
        "US_": "Amerikanska Jungfruöarna",
        "UMI": "Amerikanska mindre utliggande öar",
        "AME": "Amerikanska Samoa",
        "AND": "Andorra",
        "AGL": "Angola",
        "ANG": "Anguilla",
        "ATA": "Antarktis",
        "ANT": "Antigua och Barbuda",
        "ARG": "Argentina",
        "ARM": "Armenien",
        "ARU": "Aruba",
        "AUS": "Australien",
        "AZE": "Azerbajdzjan",
        "BMS": "Bahamas",
        "BAH": "Bahrain",
        "BAN": "Bangladesh",
        "BAR": "Barbados",
        "BGM": "Belgien",
        "BEL": "Belize",
        "BEN": "Benin",
        "BER": "Bermuda",
        "BTN": "Bhutan",
        "BOL": "Bolivia",
        "BOS": "Bosnien och Hercegovina",
        "BOT": "Botswana",
        "BVT": "Bouvetön",
        "BRA": "Brasilien",
        "BRI": "Brittiska Jungfruöarna",
        "BRU": "Brunei",
        "BUL": "Bulgarien",
        "BKF": "Burkina Faso",
        "BUR": "Burundi",
        "CAY": "Caymanöarna",
        "CEN": "Centralafrikanska Republiken",
        "CHA": "Chad",
        "CHL": "Chile",
        "CCK": "Cocos (Keeling)-öarna",
        "COL": "Colombia",
        "COO": "Cooköarna",
        "COS": "Costa Rica",
        "CYP": "Cypern",
        "DEN": "Danmark",
        "COD": "Demokratiska republiken Kongo",
        "DJI": "Djibouti",
        "DOM": "Dominica",
        "DRP": "Dominikanska republiken",
        "ECU": "Ecuador",
        "EGY": "Egypten",
        "EQU": "Ekvatorialguinea",
        "EL_": "El Salvador",
        "COT": "Elfenbenskusten",
        "ERI": "Eritrea",
        "EST": "Estland",
        "ETH": "Etiopien",
        "FLK": "Falklandsöarna",
        "FAR": "Färöarna",
        "FIJ": "Fiji",
        "PHI": "Filippinerna",
        "FIN": "Finland",
        "UAE": "Förenade Arabemiraten",
        "FRA": "Frankrike",
        "FRE": "Franska Guyana",
        "PYF": "Franska Polynesien",
        "ATF": "Franska sydterritorierna",
        "GAB": "Gabon",
        "GAM": "Gambia",
        "GEO": "Georgien",
        "GHA": "Ghana",
        "GIB": "Gibraltar",
        "GRC": "Grekland",
        "GRE": "Grenada",
        "GRL": "Grönland",
        "GDL": "Guadeloupe",
        "GUM": "Guam",
        "GUA": "Guatemala",
        "GGY": "Guernsey",
        "GUI": "Guinea",
        "GBS": "Guinea-Bissau",
        "GUY": "Guyana",
        "HAI": "Haiti",
        "HMD": "Heard- och McDonaldsöarna",
        "HON": "Honduras",
        "HKG": "Hongkong",
        "IND": "Indien",
        "IDS": "Indonesien",
        "IRA": "Irak",
        "IRN": "Iran",
        "IRE": "Irländska Republiken",
        "ICE": "Island",
        "IMN": "Isle of Man",
        "ISR": "Israel",
        "ITA": "Italien",
        "JAM": "Jamaica",
        "JAP": "Japan",
        "YEM": "Jemen",
        "JEY": "Jersey",
        "JOR": "Jordanien",
        "CXR": "Julön",
        "CAM": "Kambodia",
        "CMR": "Kamerun",
        "CAN": "Kanada",
        "CAP": "Kap Verde",
        "KAZ": "Kazakstan",
        "KEN": "Kenya",
        "CHN": "Kina",
        "KYR": "Kirgizistan",
        "KIR": "Kiribati",
        "COM": "Komorerna",
        "CRO": "Kroatien",
        "CUB": "Kuba",
        "KUW": "Kuwait",
        "LAO": "Laos",
        "LES": "Lesotho",
        "LAT": "Lettland",
        "LEB": "Libanon",
        "LIB": "Liberia",
        "LBY": "Libyen",
        "LIE": "Liechtenstein",
        "LIT": "Litauen",
        "LUX": "Luxemburg",
        "MAC": "Macao",
        "MAD": "Madagascar",
        "MKD": "Makedonien",
        "MLW": "Malawi",
        "MLS": "Malaysia",
        "MAL": "Maldiverna",
        "MLI": "Mali",
        "MLT": "Malta",
        "MOR": "Marocko",
        "MHL": "Marshallöarna",
        "MAR": "Martinique",
        "MRT": "Mauretanien",
        "MAU": "Mauritius",
        "MYT": "Mayotte",
        "MEX": "Mexiko",
        "FSM": "Mikronesien",
        "MOZ": "Moçambique",
        "MOL": "Moldavien",
        "MCO": "Monaco",
        "MON": "Mongoliet",
        "MNE": "Montenegro",
        "MTT": "Montserrat",
        "MYA": "Myanmar",
        "NAM": "Namibia",
        "NED": "Nederländerna",
        "NET": "Nederländska Antillerna",
        "NEP": "Nepal",
        "NIC": "Nicaragua",
        "NIG": "Niger",
        "NGR": "Nigeria",
        "NIU": "Niue",
        "KOR": "Nordkorea",
        "NFK": "Norfolkön",
        "NWY": "Norge",
        "MNP": "Norra Marianaöarna",
        "CDN": "Nya Kaledonien",
        "NEW": "Nya Zeeland",
        "OMA": "Oman",
        "AUT": "Österrike",
        "TLS": "Östtimor",
        "PAK": "Pakistan",
        "PLW": "Palau",
        "PSE": "Palestina",
        "PAN": "Panama",
        "PAP": "Papua Nya Guinea",
        "PAR": "Paraguay",
        "PER": "Peru",
        "PCN": "Pitcairn",
        "POL": "Polen",
        "POR": "Portugal",
        "PUE": "Puerto Rico",
        "QAT": "Qatar",
        "CON": "Republiken Kongo",
        "REU": "Reunion",
        "ROM": "Rumänien",
        "RWA": "Rwanda",
        "RUS": "Ryssland",
        "SPM": "Saint-Pierre och Miquelon",
        "SAN": "San Marino",
        "SHN": "Sankta Helena",
        "SAO": "São Tomé och Príncipe",
        "SAU": "Saudiarabien",
        "SWI": "Schweiz",
        "SEN": "Senegal",
        "YUG": "Serbien",
        "SRI": "Serbien",
        "SEY": "Seychellerna",
        "SIE": "Sierra Leone",
        "SIN": "Singapore",
        "SLO": "Slovakien",
        "SLV": "Slovenien",
        "SOL": "Solomonöarna",
        "SOM": "Somalia",
        "SPA": "Spanien",
        "LK": "Sri Lanka",
        "SKN": "St. Kitts och Nevis",
        "SLU": "St. Lucia",
        "ST.": "St. Vincent And The Grena",
        "GBR": "Storbritannien",
        "SUD": "Sudan",
        "SUR": "Surinam",
        "SJM": "Svalbard och Jan Mayen",
        "SWA": "Swaziland",
        "SOU": "Sydafrika",
        "SGS": "Sydgeorgien och Sydsandwhichöarna",
        "SKO": "Sydkorea",
        "SYR": "Syrien",
        "TWN": "Taiwan",
        "TAJ": "Tajikistan",
        "TAN": "Tanzania",
        "THA": "Thailand",
        "CZE": "Tjeckien",
        "TOG": "Togo",
        "TKL": "Tokelau",
        "TON": "Tonga",
        "TRI": "Trinidad och Tobago",
        "TUN": "Tunisien",
        "TUR": "Turkiet",
        "TKM": "Turkmenistan",
        "TCI": "Turks- och Caicosöarna",
        "TUV": "Tuvalu",
        "GER": "Tyskland",
        "UGA": "Uganda",
        "UKR": "Ukraina",
        "HUN": "Ungern",
        "UNI": "United States Of America",
        "URU": "Uruguay",
        "UZB": "Uzbekistan",
        "VAN": "Vanuatu",
        "ESH": "Västsahara",
        "WES": "Västsamoa",
        "VAT": "Vatikanstaten",
        "VEN": "Venezuela",
        "VIE": "Vietnam",
        "BLR": "Vitryssland",
        "WLF": "Wallis- och Futunaöarna",
        "ZAM": "Zambia",
        "ZIM": "Zimbabwe"
    };
    optionsFill($(".form-row__field_country"), countries_list);

    //===== GLOBAL SELECTS CUSTOMIZING =================================================================================
    // Selects text is grey by default and becomes black when any non-default option is selected
    $('.fsect').on('change', 'select', function () {
        var textColor = ($(this).val() !== '') && ($(this).val() !== '0') ? '#000' : '#BBB';
        $(this).css('color', textColor);
    });

    //===== SHOW AND HIDE FAQ BLOCKS ===================================================================================
    $('.faq__switch').click(function () {
        var $faqList = $(this).parents(".faq").find(".faq__list");
        if ( $faqList.is(':visible') ) {
            $faqList.hide("fast");
            if ( $(this).parents('.faq_terms').length === 0 ) {
                $(this).text("Öppna vanliga frågor och svar");
            }
        } else {
            $faqList.show("fast");
            if ($(this).parents('.faq_terms').length === 0) {
                $(this).text("Stäng vanliga frågor och svar");
            }
        }
        return false;
    });

    // //===== NOT KNOWN ADDRESSES =====================================================================================
    // // Hide wife address row if it's not known.
    // $('#wife_unknown_living').change(function() {
    //     var $allWifeInputs = $('.form-row_wife-address input');
    //     $allWifeInputs.prop('disabled', $(this).is(':checked'));
    //     if ($(this).is(':checked')) {
    //         $allWifeInputs.removeAttr('placeholder').val('');
    //         if ($("#husband_rb_y").is(':checked')) {
    //             $('.form-row_husband-address input').removeAttr('placeholder').val('');
    //         }
    //     } else {
    //         $('.form-row_wife-address #wife_street_address, ' +
    //             '.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
    //         $('.form-row_wife-address #wife_postal_address1, ' +
    //             '.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
    //         $('.form-row_wife-address #wife_postal_address2, ' +
    //             '.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');
    //     }
    // });
    // // Hide husband address row if it's not known.
    // $('#husband_unknown_living').change(function() {
    //     $('.form-row_husband-address input').prop('disabled', $(this).is(':checked'));
    //     if ($(this).is(':checked')) {
    //         $('.form-row_husband-address input').removeAttr('placeholder');
    //         $('.form-row_husband-address input').val('');
    //     } else {
    //         $('.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
    //         $('.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
    //         $('.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');
    //     }
    // });

    // //===== HUSBAND ADDRESS HANDLING ===================================================================================
    // // Control of husband address depending of "the same address as wife" radiobutton.
    // // $('#husband-section').find('.husband_address_group').attr('disabled', 'disabled');
    // var $husbandrbWasChecked = false;
    // $("input[name='husband_rb']").change(function(){
    //     if ($(this).val() === 'y') {        // Husband has same address as wife.
    //         $husbandrbWasChecked = true;
    //         $('#husband-section').find('.husband_address_group').attr('disabled', 'disabled');
    //         $('#husband_street_address').val($('#wife_street_address').val());
    //         $('#husband_postal_address1').val($('#wife_postal_address1').val());
    //         $('#husband_postal_address2').val($('#wife_postal_address2').val());
    //
    //         var $h_mun = $('#husband_city');
    //         $h_mun.val($('#wife_city').val());
    //         if ( $h_mun.val() !== '' ) {
    //             $h_mun.css('color', '#888');
    //         } else {
    //             $h_mun.css('color', '#BBB');
    //         }
    //         if ($('#pageForm').data("formtype") === 'utlands') {
    //             var $h_country = $('#husband_country');
    //             $h_country.val($('#wife_country').val());
    //             if ( $h_country.val() !== '' ) {
    //                 $h_country.css('color', '#888');
    //             } else {
    //                 $h_country.css('color', '#BBB');
    //             }
    //             if ($h_country.val() === 'SWE') {
    //                 $h_mun.parents('.form-row__field-wrapper').show();
    //             } else {
    //                 $h_mun.parents('.form-row__field-wrapper').hide();
    //             }
    //
    //             //
    //             $('.form-row_husband-addr-unknown').hide(300);
    //             if ($('#wife_unknown_living').is(':checked')) {
    //                 $('.form-row_husband-address *').removeAttr('placeholder');
    //             } else {
    //                 $('.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
    //                 $('.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
    //                 $('.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');
    //             }
    //         }
    //     } else if ($(this).val() === 'n') {         // Husband's address differs from wife's one.
    //         $('#husband-section').find('.husband_address_group').removeAttr('disabled');
    //         if ($husbandrbWasChecked) {
    //             $('.husband_address_group').val('');
    //             $('#husband_country').val('0');
    //             $('#husband_city').val('0');
    //         }
    //         if ($('#pageForm').data("formtype") === 'utlands') {
    //             $('#husband_city').parents('.form-row__field-wrapper').hide();
    //
    //             $('.form-row_husband-addr-unknown').show(300);
    //             //
    //             $('.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
    //             $('.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
    //             $('.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');
    //
    //             $('#husband_unknown_living').attr('checked', false);
    //         }
    //     }
    // });
    // // Dynamically change husband address if it's the same as wife address and wife address is changed
    // $('#wife_street_address, #wife_postal_address1, #wife_postal_address2').keyup( function () {
    //     if ($('#husband_rb_y:checked').length !== 0) {
    //         $('#husband_street_address').val( $('#wife_street_address').val() );
    //         $('#husband_postal_address1').val( $('#wife_postal_address1').val() );
    //         $('#husband_postal_address2').val( $('#wife_postal_address2').val() );
    //     }
    // });
    // $('#wife_country').change( function () {
    //     if ($('#husband_rb_y:checked').length !== 0) {
    //         var $h_country = $('#husband_country');
    //         $h_country.val( $('#wife_country').val() );
    //         if ( $h_country.val() !== '' ) {
    //             $h_country.css('color', '#888');
    //         } else {
    //             $h_country.css('color', '#BBB');
    //         }
    //     }
    // });
    // $('#wife_city').change( function () {
    //     if ($('#husband_rb_y:checked').length !== 0) {
    //         var $h_mun = $('#husband_city');
    //         $h_mun.val( $('#wife_city').val() );
    //         if ( $h_mun.val() !== '' ) {
    //             $h_mun.css('color', '#888');
    //         } else {
    //             $h_mun.css('color', '#BBB');
    //         }
    //     }
    // });

    //===== HUSBAND ADDRESS HANDLING ===================================================================================
    // Control of husband address depending of "the same address as wife" checkbox.
    var $husbandchbWasChecked = false;
    $("#husband_chb").change(function(){
        if ($(this).is(':checked')) {        // Husband has same address as wife.
            $husbandchbWasChecked = true;
            $('#husband-section').find('.husband_address_group').attr('disabled', 'disabled');
            $('#husband_street_address').val($('#wife_street_address').val());
            $('#husband_postal_address1').val($('#wife_postal_address1').val());
            $('#husband_postal_address2').val($('#wife_postal_address2').val());

            var $h_mun = $('#husband_city');
            $h_mun.val($('#wife_city').val());
            if ( $h_mun.val() !== '' ) {
                $h_mun.css('color', '#888');
            } else {
                $h_mun.css('color', '#BBB');
            }
            if ($('#pageForm').data("formtype") === 'utlands') {
                var $h_country = $('#husband_country');
                $h_country.val($('#wife_country').val());
                if ( $h_country.val() !== '' ) {
                    $h_country.css('color', '#888');
                } else {
                    $h_country.css('color', '#BBB');
                }
                if ($h_country.val() === 'SWE') {
                    $h_mun.parents('.form-row__field-wrapper').show();
                } else {
                    $h_mun.parents('.form-row__field-wrapper').hide();
                }

                //
                $('.form-row_husband-addr-unknown').hide(300);
                if ($('#wife_unknown_living').is(':checked')) {
                    $('.form-row_husband-address *').removeAttr('placeholder');
                } else {
                    $('.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
                    $('.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
                    $('.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');
                }
            }
        } else {                            // Husband's address differs from wife's one.
            $('#husband-section').find('.husband_address_group').removeAttr('disabled');
            if ($husbandchbWasChecked) {
                $('.husband_address_group').val('');
                $('#husband_country').val('0');
                $('#husband_city').val('0');
            }
            if ($('#pageForm').data("formtype") === 'utlands') {
                $('#husband_city').parents('.form-row__field-wrapper').hide();

                $('.form-row_husband-addr-unknown').show(300);
                //
                $('.form-row_husband-address #husband_street_address').attr('placeholder', 'Riksvagen 45');
                $('.form-row_husband-address #husband_postal_address1').attr('placeholder', '107 55');
                $('.form-row_husband-address #husband_postal_address2').attr('placeholder', 'Stockholm');

                $('#husband_unknown_living').attr('checked', false);
            }
        }
    });
    // Dynamically change husband address if it's the same as wife address and wife address is changed
    $('#wife_street_address, #wife_postal_address1, #wife_postal_address2').keyup( function () {
        if ($('#husband_chb').is(':checked')) {
            $('#husband_street_address').val( $('#wife_street_address').val() );
            $('#husband_postal_address1').val( $('#wife_postal_address1').val() );
            $('#husband_postal_address2').val( $('#wife_postal_address2').val() );
        }
    });
    $('#wife_country').change( function () {
        if ($('#husband_chb').is(':checked')) {
            var $h_country = $('#husband_country');
            $h_country.val( $('#wife_country').val() );
            if ( $h_country.val() !== '' ) {
                $h_country.css('color', '#888');
            } else {
                $h_country.css('color', '#BBB');
            }
        }
    });
    $('#wife_city').change( function () {
        if ($('#husband_chb').is(':checked')) {
            var $h_mun = $('#husband_city');
            $h_mun.val( $('#wife_city').val() );
            if ( $h_mun.val() !== '' ) {
                $h_mun.css('color', '#888');
            } else {
                $h_mun.css('color', '#BBB');
            }
        }
    });

    //===== FORM 3 ONLY ================================================================================================
    if ($('#pageForm').data("formtype") === 'utlands') {

        //======APPEARANCE OF 'KOMMUN' FIELD ===========================================================================
        // Appearance of "Kommun" field.
        var $w_mun = $('#wife_city');
        var $h_mun = $('#husband_city');
        // Initially "Kommun" field is hidden. It's showed if value of "Land" is "SWE"(Sverige)
        $w_mun.parents('.form-row__field-wrapper').hide();
        $h_mun.parents('.form-row__field-wrapper').hide();
        $('#wife_country').change(function () {
            if ($('#wife_country').val() === 'SWE') {
                $w_mun.parents('.form-row__field-wrapper').show();
                if ($('#husband_rb_y:checked').length !== 0) {
                    $h_mun.parents('.form-row__field-wrapper').show();
                }
            } else {
                $w_mun.parents('.form-row__field-wrapper').hide();
                if ($('#husband_rb_y:checked').length !== 0) {
                    $h_mun.parents('.form-row__field-wrapper').hide();
                }
            }
        });
        $('#husband_country').change(function () {
            if ($('#husband_country').val() === 'SWE') {
                $h_mun.parents('.form-row__field-wrapper').show();
            } else {
                $h_mun.parents('.form-row__field-wrapper').hide();
            }
        });

        //===== PERIODS OF LIVING IN COUNTRIES =========================================================================
        // Add period
        $('.add-period-wife').click(function () {
            var periodId = $('.period-wife').last().data("period-id") + 1;
            var periodInstanceText = $('#period-wife-template').text().replace(/\{\{N\}\}/g, periodId);
            $('.periods-wife').append(periodInstanceText);
            dynamicValidate($(this).attr('id'));

            // Selects filling
            optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_country'), countries_list);
            optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_year'), years_list);
            optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_month'), months_list);

            return false;
        });
        // Remove period, then validate
        $('.periods-wife').on('click', '.remove-period-wife', function () {
            var $period = $(this).parents('.period-wife');
            $period.remove();
            validate([validatorsDynamic['wife-section']])();     // Validation should be performed after removing period.
            return false;
        });
        // Add period
        $('.add-period-husband').click(function () {
            var periodId = $('.period-husband').last().data("period-id") + 1;
            var periodInstanceText = $('#period-husband-template').text().replace(/\{\{N\}\}/g, periodId);
            $('.periods-husband').append(periodInstanceText);
            dynamicValidate($(this).attr('id'));

            // Selects filling
            optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_country'), countries_list);
            optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_year'), years_list);
            optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_month'), months_list);

            return false;
        });
        // Remove period, then validate
        $('.periods-husband').on('click', '.remove-period-husband', function () {
            var $period = $(this).parents('.period-husband');
            $period.remove();
            validate([validatorsDynamic['husband-section']])();     // Validation should be performed after removing period.
            return false;
        });
    }

    //===== CHILDREN-RELATED SECTIONS ==================================================================================
    // At the beginning all sections related to children are hidden and disabled
    var $childrenSections = $('#common-children-section, #custody-section, #living-section');
    $childrenSections.hide();
    $childrenSections.find('*').attr('disabled', 'disabled');
    $('.add-child').hide();
    // User must choose if there are common children
    $("input[name='children_rb']").click(function() {
        if ($(this).val() === 'y') {
            // Enable fields for children data entering
            // (they are disabled before the first click on 'yes')
            $('#common-children-section').find('*').removeAttr('disabled');
            // Show these fields if they were hidden (second and subsequent clicks on 'yes')
            $('#common-children-section, #custody-section, #living-section, .add-child').show("fast");
        } else if ($(this).val() === 'n') {
            $('#common-children-section, #custody-section, #living-section').hide("fast");
            $('#common-children-section, #custody-section, #living-section').find('input').val('');
        }
    });

    // Add common child
    $('.add-child').click(function () {
        var childId = $('.child').last().data("child-id") + 1;
        var childInstanceText = $('#child-template').text().replace(/\{\{N\}\}/g, childId);
        $('.children').append(childInstanceText);

        // Selects filling
        optionsFill($('.children').find('.child').last().find('.form-row__field_year'), years_list);
        optionsFill($('.children').find('.child').last().find('.form-row__field_month'), months_list);
        optionsFill($('.children').find('.child').last().find('.form-row__field_day'), days_list);

        return false;
    });
    // Remove common child and corresponding radiobuttons in "custody" and "living" sections, then validate
    $('.children').on('click', '.remove-child', function () {
        var $child = $(this).parents('.child');
        var childId = $child.data("child-id");
        // Select radios corresponding to current child
        var $childRadios = $('.child-radio[data-child-id="' + childId + '"]');
        $childRadios.remove();
        // Validation should be performed after removing radios but before removing child
        // (for resetting validation errors associated with this child if they were set).
        dynamicValidate($(this).attr('id'));
        $child.remove();
        validate([validatorsDynamic['common-children-section']])();
        return false;
    });

    // Dynamically add children names (entered in "Common children" section) to "Custody" and "Living" sections.
    $("#common-children-section").on("keyup", ".form-row__field_child-fname, .form-row__field_child-lname", function () {
        var $sections = $('#custody-section, #living-section'); // Sections to add or remove radios

        var $child = $(this).parents('.child');
        var childId = $child.data("child-id");
        var childFname = $child.find('.form-row__field_child-fname').val();
        var childLname = $child.find('.form-row__field_child-lname').val();
        // Select radios corresponding to current child
        var $childRadios = $('.child-radio[data-child-id="' + childId + '"]');

        // Show corresponding child radiobutton in 'custody' and 'living' sections if both firstname and lastname are filled
        if (childFname !== '' && childLname !== '') {
            if ($childRadios.length !== 0) {
                // If this child radios are already created - change text in labels and show if they were hidden:
                $childRadios.find('.radio__label').text(childFname + ' ' + childLname);
                $childRadios.show();
            } else {
                // If this child radios are absent - create them:
                var radioCustodyWifeStr = $('#child-radio-custody-template-wife').text().replace(/\{\{N\}\}/g, childId);
                var $radioCustodyWife = $($.parseHTML(radioCustodyWifeStr));
                $radioCustodyWife.find('.radio__label').text(childFname + ' ' + childLname);
                $('#custody-section').find('.form-row_indented_wife').append($radioCustodyWife);

                var radioCustodyHusbandStr = $('#child-radio-custody-template-husband').text().replace(/\{\{N\}\}/g, childId);
                var $radioCustodyHusband = $($.parseHTML(radioCustodyHusbandStr));
                $radioCustodyHusband.find('.radio__label').text(childFname + ' ' + childLname);
                $('#custody-section').find('.form-row_indented_husband').append($radioCustodyHusband);

                var radioLivingWifeStr = $('#child-radio-living-template-wife').text().replace(/\{\{N\}\}/g, childId);
                var $radioLivingWife = $($.parseHTML(radioLivingWifeStr));
                $radioLivingWife.find('.radio__label').text(childFname + ' ' + childLname);
                $('#living-section').find('.form-row_indented_wife').append($radioLivingWife);

                var radioLivingHusbandStr = $('#child-radio-living-template-husband').text().replace(/\{\{N\}\}/g, childId);
                var $radioLivingHusband = $($.parseHTML(radioLivingHusbandStr));
                $radioLivingHusband.find('.radio__label').text(childFname + ' ' + childLname);
                $('#living-section').find('.form-row_indented_husband').append($radioLivingHusband);
            }
        } else {
            if ($childRadios.length !== 0) {
                $childRadios.hide();
            }
        }

        // Disable fields in "custody" and "living" sections when there is no children names entered
        if ($sections.find($('.child-radio').filter(':visible') ).length !== 0) {
            $sections.find('*').removeAttr('disabled');
        } else {
            $sections.find('*').attr('disabled', 'disabled');
        }
    });

    // Higher and lower level radios interaction in "custody" and "living" sections.
    // If checked radio is lower-level, last higher-level radio should also be checked.
    // If checked radio is higher-level and it's not last, all lower-level radios should be cleared.
    $('#custody-section, #living-section').on('click', '.radio__label', function () {
        var $formRow = $(this).parents('.form-row');
        var nextRowIsNotIndented = $formRow.next('.form-row_indented').length === 0;
        if ($formRow.hasClass('form-row_indented')) {
            $formRow.prevAll('.form-row:not(.form-row_indented):first').find('input').prop( "checked", true );
        } else {
            if ( nextRowIsNotIndented ) {
                $formRow.nextAll('.form-row_indented').find('input').prop("checked", false);
            }
        }
    });

    // Single-custody select is black if corresponding radiobutton is checked and grey otherwise
    $('[name="custody"]').click(function () {
        var textColor = ($(this).val() === 'single') ? '#000' : '#BBB';
        $('#single_custody_who').css('color', textColor);
    });
    // Check single-custody radiobutton when corresponding select is clicked
    $('#single_custody_who').click(function () {
        $('#custody-single').prop('checked', true);
        $(this).css('color', '#000');
        dynamicValidate($(this).attr('id'));
    });

    //====== "HOUSE" CHECKBOX CLICK CONTROLS THE COLOR OF "HOUSE" SELECT ===============================================
    $('#house').click(function () {
        var textColor = $(this).is(':checked') ? '#000' : '#BBB';
        $('#who_house').css('color', textColor);
    });


    //===================== VALIDATION =================================================================================

    //===== GLOBAL VALIDATION VARIABLES ================================================================================
    var submitAttempted = false;    // If validate button has been pressed at least once.
    var submitValidation = false;   // If current validation is performed on submit button click.
    var $firstErrorField = null;    // First non-valid field in form.

    //===== FORM SUBMISSION - CLICK ON "BEKRAFTA KOP" BUTTON ===========================================================
    $('#submitButton').click(function() {
        if ( submitValidate() ) {
            // Disabled fields must be enabled for passing their values to order:
            $('#husband-section').find('.husband_address_group').removeAttr('disabled');
            // Prevent after-submit form editing by user:
            $('.disable-cover').show();
            return true;
        }
        return false;
    });

    //===== HIGHEST-LEVEL VALIDATION FUNCTIONS =========================================================================

    // Validators for sections
    var validator_wife = [validateWife];
    var validator_husband = [validateHusband];
    var validator_children = [
        validate([
            [validateFieldFilled('children_rb'), displayDiv('children_rb_error')],
        ]),
        changeHeaderIcon('children')
    ];
    var validator_common_children = [validateCommonChildren];
    var validator_custody = [validateCustody];
    var validator_living = [validateLiving];
    var validator_waiting = [
        validate([
            [validateFieldFilled('think'), displayDiv('think_error')],
            [validateFieldFilled('living_together'), displayDiv('living_together_error')],
            [validateFieldFilled('childs_bellow_16'), displayDiv('childs_bellow_16_error')],
        ]),
        changeHeaderIcon('waiting')
    ];
    var validator_marriage = [];
    if ($('#marriage-section').length !== 0) {  // If section "marriage" exists
        validator_marriage = [
            validate([
                [validateFieldFilled('marriage_country'), displayDiv('marriage_country_error')],
                [validateFieldFilled('registered_in_sweden'), displayDiv('registered_in_sweden_error')]
            ]),
            changeHeaderIcon('marriage')
        ];
    }
    var validator_comfirm = [
        validate([
            [validateFieldFilled('payment'), displayDiv('payment-pay-error')],
            [validateFieldFilled('cellphone'), displayDiv('cellphone_error')],
            [validateFieldFilled('email'), displayDiv('email_error')],
            [validateFieldFilled('confirm'), displayDiv('confirm_error')]
        ])
    ];

    // Accordance between sections names and validators
    var validatorsDynamic = {
        'wife-section': validator_wife,
        'husband-section': validator_husband,
        'children-section': validator_children,
        'common-children-section': validator_common_children,
        'custody-section': validator_custody,
        'living-section': validator_living,
        'waiting-section': validator_waiting,
        'marriage-section': validator_marriage,
        'confirm-section': validator_comfirm
    };

    // Dynamic validate event binding.
    $('.fsect').on('keyup', 'input[type=text]', function () {
        dynamicValidate($(this).attr('id'));
    });
    $('.fsect').on('change', 'input[type=checkbox], input[type=radio], select', function () {
        dynamicValidate($(this).attr('id'));
    });
    $('.add-child').click(function () {
        dynamicValidate($(this).attr('id'));
    });
    $('.add-period-wife').click(function () {
        dynamicValidate($(this).attr('id'));
    });

    // Dynamic validation is performed at any input field change.
    function dynamicValidate(fieldId) {
        submitValidation = false;
        var sectionId = $('#' + fieldId).parents('.fsect').attr('id');
        switch (sectionId) {
            case 'wife-section':
            case 'husband-section':
                validate([validatorsDynamic['wife-section']])();
                validate([validatorsDynamic['husband-section']])();
                break;
            case 'children-section':
                validate([validatorsDynamic['children-section']])();
            case 'common-children-section':
            case 'custody-section':
            case 'living-section':
                validate([validatorsDynamic['common-children-section']])();
                validate([validatorsDynamic['custody-section']])();
                validate([validatorsDynamic['living-section']])();
                break;
            case 'waiting-section':
                validate([validatorsDynamic['waiting-section']])();
                break;
            case 'marriage-section':
                validate([validatorsDynamic['marriage-section']])();
                break;
            case 'confirm-section':
                validate([validatorsDynamic['wife-section']])();        // for "wife_unknown_living"
                validate([validatorsDynamic['husband-section']])();     // for "husband_unknown_living"
                validate([validatorsDynamic['confirm-section']])();
                break;
            default: break;
        }
    }

    // Validation in "submit" button click
    function submitValidate() {
        $firstErrorField = null;    // reset before each new validation.
        submitAttempted = true;     // Changes from 'false' to 'true' only once and remains 'true'
        submitValidation = true;
        var validatorsSubmit = [
            validator_wife,
            validator_husband,
            validator_children,
            validator_common_children,
            validator_custody,
            validator_living,
            validator_waiting,
            validator_comfirm
        ];
        if ($('#marriage-section').length !== 0) {  // If section "marriage" exists
            validatorsSubmit.push(validator_marriage);
        }
        return validate(validatorsSubmit)();    // If there are any non-valid fields, the form will not be submitted.
    }

    //====== SECTIONS VALIDATION FUNCTIONS =============================================================================

    // "Wife" section validation
    function validateWife() {
        var val_arr_wife = [    // Minimal validator - always presenting fields.
            [validateFieldFilled('wife_fname'), displayDiv('wife_fname_error')],
            [validateFieldFilled('wife_lname'), displayDiv('wife_lname_error')],
        ];

        // "Personnummer" row - birth date and ssn2.
        var ssn2Ignore = false;
        if ($('#pageForm').data("formtype") === 'utlands') {    // SSN2 may be ignored in form 3 only.
            ssn2Ignore = $('#wife_country').val() !== 'SWE';
        }
        val_arr_wife.push([
            validate([
                [validateFieldFilled('wife_year')],
                [validateFieldFilled('wife_month')],
                [validateFieldFilled('wife_day')],
                // When Sweden in form 3 is selected, municipality field should be validated:
                [validateFieldFilled('wife_ssn2', ssn2Ignore)]
            ]),
            displayDiv('wife_personnummer_error')
        ]);

        // Address row - "Gatuadress", "Postnummer" and "Stad" fields.
        var unknownLiving = $('#wife_unknown_living').is(':checked');
        val_arr_wife.push(
            [validate([
                [validateFieldFilled('wife_street_address', unknownLiving)],
                [validateFieldFilled('wife_postal_address1', unknownLiving)],
                [validateFieldFilled('wife_postal_address2', unknownLiving)]
            ]), displayDiv('wife_address_error')]
        );

        if ($('#pageForm').data("formtype") === 'standard') {                   // Form 1.
            val_arr_wife.push([validateFieldFilled('wife_city'), displayDiv('wife_city_error')]);
        } else if ($('#pageForm').data("formtype") === 'stamnings') {           // Form 2.
            val_arr_wife.push([validateFieldFilled('wife_wants_divorce'), displayDiv('wife_wants_divorce_error')]);
            val_arr_wife.push([validateFieldFilled('wife_city'), displayDiv('wife_city_error')]);
        } else if ($('#pageForm').data("formtype") === 'utlands') {             // Form 3.

            var wifePays = $('#payment').val() == 'wife';
            val_arr_wife.push(
                [validateChbEmpty('wife_unknown_living', !wifePays), displayDiv('wife_unknown_living_error')]
            );

            // "Land" and "Kommun" fields.
            if ( $.trim( $('[name="wife_country"]').val() ) !== "0" ) {
                val_arr_wife.push([validateFieldFilled('wife_country', true), displayDiv('wife_country_error')]);
                val_arr_wife.push([validateSelectIsFilledAndKnown('wife_country', !wifePays), displayDiv('wife_country1_error')]);
            } else {
                val_arr_wife.push([validateSelectIsFilledAndKnown('wife_country', true), displayDiv('wife_country1_error')]);
                val_arr_wife.push([validateFieldFilled('wife_country'), displayDiv('wife_country_error')]);
            }
            // If "Kommun" field is not hidden, it should be validated.
            if ($('#wife_city:visible').length !== 0) {
                val_arr_wife.push([validateFieldFilled('wife_city'), displayDiv('wife_city_error')]);
            }

            // Periods of living in countries since marriage.
            var periodsFieldsArray = [];
            $('.period-wife').each(function () {
                var periodId = $(this).data("period-id");
                periodsFieldsArray.push([validateFieldFilled('wife_living_country_' + periodId, unknownLiving)]);
                periodsFieldsArray.push([validateFieldFilled('wife_living_from_' + periodId + '_year', unknownLiving)]);
                periodsFieldsArray.push([validateFieldFilled('wife_living_from_' + periodId + '_month', unknownLiving)]);
                periodsFieldsArray.push([validateFieldFilled('wife_living_to_' + periodId + '_year', unknownLiving)]);
                periodsFieldsArray.push([validateFieldFilled('wife_living_to_' + periodId + '_month', unknownLiving)]);
            });
            val_arr_wife.push([validate(periodsFieldsArray), displayDiv('periods_wife_error')]);

            // Sitizenship ("medborgarskap") field
            if ( $.trim( $('[name="wife_country_citizen[]"]').val() ).length !== 0 ) {
                val_arr_wife.push(
                    [validateFieldFilled('wife_country_citizen[]', true), displayDiv('wife_country_citizen_error')]
                );               // Error reset
                val_arr_wife.push(
                    [validateSelectIsFilledAndKnown('wife_country_citizen[]', !wifePays),
                        displayDiv('wife_country_citizen1_error')]
                );

            } else {
                val_arr_wife.push(
                    [validateSelectIsFilledAndKnown('wife_country_citizen[]', true),
                        displayDiv('wife_country_citizen1_error')]
                );   // Error reset.
                val_arr_wife.push([validateFieldFilled('wife_country_citizen[]'), displayDiv('wife_country_citizen_error')]);
            }

            // If wife wants divorce.
            val_arr_wife.push([validateFieldFilled('wife_wants_divorce'), displayDiv('wife_wants_divorce_error')]);
        }

        var validator = [
            validate(val_arr_wife),
            changeHeaderIcon('wife')
        ];
        return validate([validator])();
    }

    // "Husband" section validation
    function validateHusband() {
        var val_arr_husband = [    // Minimal validator - always presenting fields.
            [validateFieldFilled('husband_fname'), displayDiv('husband_fname_error')],
            [validateFieldFilled('husband_lname'), displayDiv('husband_lname_error')]
        ];

        var ssn2Ignore = false;
        if ($('#pageForm').data("formtype") === 'utlands') {    // SSN2 may be ignored in form 3 only.
            ssn2Ignore = $('#husband_country').val() !== 'SWE';
        }
        val_arr_husband.push([
            validate([
                [validateFieldFilled('husband_year')],
                [validateFieldFilled('husband_month')],
                [validateFieldFilled('husband_day')],
                // When Sweden in form 3 is selected, municipality field should be validated:
                [validateFieldFilled('husband_ssn2', ssn2Ignore)]
            ]),
            displayDiv('husband_personnummer_error')
        ]);

        // var husbandAddressDiffers = ( $('#husband_rb_n:checked').length !== 0 );
        var husbandLivesWithWife = ( $('#husband_chb').is(':checked') );
        var husbandAddressUnknown = $('#husband_unknown_living').is(':checked');
        var addressIgnore = (!husbandLivesWithWife && husbandAddressUnknown) || husbandLivesWithWife;
        val_arr_husband.push([
            validate([
                [validateFieldFilled('husband_street_address', addressIgnore)],
                [validateFieldFilled('husband_postal_address1', addressIgnore)],
                [validateFieldFilled('husband_postal_address2', addressIgnore)]
            ]), displayDiv('husband_address_error')
        ]);

        var husbandPays = $('#payment').val() == 'husband';
        val_arr_husband.push(
            [validateChbEmpty('husband_unknown_living', !husbandPays), displayDiv('husband_unknown_living_error')]
        );

        if ($('#pageForm').data("formtype") === 'standard') {                   // Form 1.
            val_arr_husband.push([validateFieldFilled('husband_city', husbandLivesWithWife), displayDiv('husband_city_error')]);
        } else if ($('#pageForm').data("formtype") === 'stamnings') {           // Form 2.
            val_arr_husband.push([validateFieldFilled('husband_wants_divorce'), displayDiv('husband_wants_divorce_error')]);
            val_arr_husband.push([validateFieldFilled('husband_city', husbandLivesWithWife), displayDiv('husband_city_error')]);
        } else if ($('#pageForm').data("formtype") === 'utlands') {             // Form 3.



            if (validateFieldFilled('husband_country')()) {
                val_arr_husband.push([validateFieldFilled('husband_country', true), displayDiv('husband_country_error')]);               // Error reset
                if (husbandPays) {
                    val_arr_husband.push([validateSelectIsFilledAndKnown('husband_country', husbandLivesWithWife), displayDiv('husband_country1_error')]);
                }
            } else {
                val_arr_husband.push([validateSelectIsFilledAndKnown('husband_country', true), displayDiv('husband_country1_error')]);   // Error reset.
                val_arr_husband.push([validateFieldFilled('husband_country', husbandLivesWithWife), displayDiv('husband_country_error')]);
            }
            // If "Kommun" field is not hidden, it should be validated.
            if ($('#husband_city:visible').length !== 0) {
                val_arr_husband.push([validateFieldFilled('husband_city', husbandLivesWithWife), displayDiv('husband_city_error')]);
            }

            var periodsFieldsArray = [];
            $('.period-husband').each(function () {
                var periodId = $(this).data("period-id");
                periodsFieldsArray.push([validateFieldFilled('husband_living_country_' + periodId, husbandAddressUnknown)]);
                periodsFieldsArray.push([validateFieldFilled('husband_living_from_' + periodId + '_year', husbandAddressUnknown)]);
                periodsFieldsArray.push([validateFieldFilled('husband_living_from_' + periodId + '_month', husbandAddressUnknown)]);
                periodsFieldsArray.push([validateFieldFilled('husband_living_to_' + periodId + '_year', husbandAddressUnknown)]);
                periodsFieldsArray.push([validateFieldFilled('husband_living_to_' + periodId + '_month', husbandAddressUnknown)]);
            });
            val_arr_husband.push([validate(periodsFieldsArray), displayDiv('periods_husband_error')]);


            if (validateFieldFilled('husband_country_citizen[]')()) {
                val_arr_husband.push([validateFieldFilled('husband_country_citizen[]', true), displayDiv('husband_country_citizen_error')]);               // Error reset
                if (husbandPays) {
                    val_arr_husband.push([validateSelectIsFilledAndKnown('husband_country_citizen[]'), displayDiv('husband_country_citizen1_error')]);
                }
            } else {
                val_arr_husband.push([validateSelectIsFilledAndKnown('husband_country_citizen[]', true), displayDiv('husband_country_citizen1_error')]);   // Error reset.
                val_arr_husband.push([validateFieldFilled('husband_country_citizen[]'), displayDiv('husband_country_citizen_error')]);
            }

            val_arr_husband.push([validateFieldFilled('husband_wants_divorce'), displayDiv('husband_wants_divorce_error')]);
        }

        var validator = [
            validate(val_arr_husband),
            changeHeaderIcon('husband')
        ];
        return validate([validator])();
    }

    // "Common children" section validation
    function validateCommonChildren() {
        // If there is no common children - no validation needed
        if ($("input[name='children_rb']:checked").val() !== 'y') {
            return true;
        }
        var val_arr_common_children = [];
        $('.child').each(function () {
            var childId = $(this).data("child-id");
            val_arr_common_children.push([validateFieldFilled('child_' + childId + '_fname'), displayDiv('child_' + childId + '_fname_error')]);
            val_arr_common_children.push([validateFieldFilled('child_' + childId + '_lname'), displayDiv('child_' + childId + '_lname_error')]);
            val_arr_common_children.push([
                validate([
                    [validateFieldFilled('child_' + childId + '_year')],
                    [validateFieldFilled('child_' + childId + '_month')],
                    [validateFieldFilled('child_' + childId + '_day')],
                    [validateFieldFilled('child_' + childId + '_ssn2')]
                ]),
                displayDiv('child_' + childId + '_personnummer_error')
            ]);
        });
        var validator = [
            validate(val_arr_common_children),
            changeHeaderIcon('common-children')
        ];
        return validate([validator])();
    }

    // "Custody" section validation
    function validateCustody() {
        // If there is no common children - no validation needed
        if ($("input[name='children_rb']:checked").val() !== 'y') {
            return true;
        }

        // Check by the first radiobutton if all radiobuttons are enabled (i.e. there is at least one fully entered child):
        if ($('#custody-consent:disabled').length === 0) {

            var val_arr_custody = [];
            // Iterating by children
            $('.child').each(function () {
                var childId = $(this).data("child-id");

                // Add lower-level radiobuttons validation
                // if corresponding lower-level radiobutton pair visible (i.e. child firstname and lastname are filled)
                var rbPairVisible = ($('[for="child_' + childId + '_custody-wife"]:visible').length !== 0);
                if (rbPairVisible) {
                    // If corresponding higher-level radiobutton is not checked - validation will return 'true' (needed for error reset)
                    var custodyDividedChecked = ( $('#custody-divided:checked').length !== 0 );
                    val_arr_custody.push([validateFieldFilled( 'child_' + childId + '_custody', !custodyDividedChecked )]);
                }
            });

            var validator = [
                validate([
                    [validateFieldFilled('custody'), displayDiv('custody_error')],
                    [validate(val_arr_custody), displayDiv('custody_error1')]
                ]),
                changeHeaderIcon('custody')
            ];
            return validate([validator])();
        }
        return true;
    }

    // "Living" section validation
    function validateLiving() {
        // If there is no common children - no validation needed
        if ($("input[name='children_rb']:checked").val() !== 'y') {
            return true;
        }

        var val_arr_living = [];
        var livingErrorIds = ['living_error2', 'living_error3', 'living_error4', 'living_error5'];
        var livingError = '';
        var clearFields = [];

        // Check by the first radiobutton if all radiobuttons are enabled (i.e. there is at least one fully entered child):
        if ($('#living-consent:disabled').length === 0) {

            $('.child').each(function () {
                var childId = $(this).data("child-id");

                if ($('#living-divided:checked').length !== 0) {        // 2nd radio is checked

                    var $livingIndentedInputs = $('#living-section .form-row_indented input');
                    var allLivingIndentedChecked =
                        ( $livingIndentedInputs.filter(':checked').length === $livingIndentedInputs.filter(':not(:checked)').length );

                    if ( !allLivingIndentedChecked ) {                  // Not all indented inputs are checked
                        val_arr_living.push( [validateFieldFilled('child_' + childId + '_living')] );
                        livingError = 'living_error5';

                    } else {                                            // All indented inputs are checked

                        var $custodyIndentedInputs = $('#custody-section .form-row_indented input');
                        var allCustodyIndentedChecked =
                            ( $custodyIndentedInputs.filter(':checked').length === $custodyIndentedInputs.filter(':not(:checked)').length );

                        if ($('#custody-single:checked').length !== 0) {                // Custody: 2nd radio is checked
                            if ($('#single_custody_who').val() === 'wife') {            // Custody: wife selected
                                val_arr_living.push( [validateRbChecked('child_' + childId + '_living-wife')] );
                                livingError = 'living_error2';
                            } else if ($('#single_custody_who').val() === 'husband') {  // Custody: husband selected
                                val_arr_living.push([validateRbChecked('child_' + childId + '_living-husband')]);
                                livingError = 'living_error3';
                            }
                        }

                        // Custody: all indented radios are checked
                        else if (allCustodyIndentedChecked) {
                            // If child checkboxes are not removed
                            if ( $('[name=child_' + childId + '_custody]').length !== 0 ) {
                                // All radios in 'living' checked should be the same as in 'custody'
                                var childCustodyVal = $('[name=child_' + childId + '_custody]:checked').val();
                                var childLivingId = $('[name=child_' + childId + '_living]').filter('[value=' + childCustodyVal + ']').attr('id');
                                val_arr_living.push([validateRbChecked(childLivingId)]);
                                livingError = 'living_error4';
                            }
                        }
                    }
                }
            });

            //
            var validateArg = [];
            // Create array of all radio names (i.e. groups) in 'living' for clear errors except one recently enabled
            clearFields.push( [validateFieldFilled("living", true) ]); // Permanent highel-level rb group.
            $('#living-section .form-row_indented:first input').each(function () {
                var current_name = $(this).attr('name');
                clearFields.push( [validateFieldFilled(current_name, true)] );
            });

            //
            livingErrorIds.forEach(function (item, i, arr) {
                if (item !== livingError) {
                    validateArg.push( [validate(clearFields), displayDiv(item)] );
                }
            });
            validateArg.push( [validateFieldFilled('living'), displayDiv('living_error')] );
            if (val_arr_living.length !== 0) {
                validateArg.push( [validate(val_arr_living), displayDiv(livingError)] );
            }

            //
            var validator = [
                validate(
                    validateArg
                ),
                changeHeaderIcon('living')
            ];
            return validate([validator])();
        }
        return true;
    }

    //====== FIELDS VALIDATION FUNCTIONS ===============================================================================

    // Scrolling to first error field and focusing on it (after click on "submit" button)
    function scrollAndFocus() {
        // If validation is performed by clicking "submit" button -
        // smoothly scroll to 30px higher form first error field and focus on this field:
        if (submitValidation) {
            $('html, body').animate(
                {scrollTop: $firstErrorField.offset().top - 30},
                1000,
                function() { $firstErrorField.focus(); }
            );
        }
    }

    // Checks if field is filled: text is entered, select option is non-default,
    // radio group has checked item, checkbox is checked.
    function validateFieldFilled(fieldName, ignore) {
        return function () {
            var $field = $('[name="' + fieldName + '"]');
            var inputType = $field.attr('type');

            // If this is a radiobutton or checkbox:
            if ( inputType == 'radio' || inputType == 'checkbox' ) {

                // if no button/checkbox is checked and field should not be ignored:
                if ($('[name=' + fieldName + ']:checked').length === 0 && !ignore) {
                    if (submitAttempted) {              // Error is showed if 'submit' button has already been clicked.
                        if (!$firstErrorField) {                                    // if it's the first error in form
                            // Point $firstErrorField to label of this element
                            // (because radiobutton/checkbox is hidden and can't be scrolled to):
                            $firstErrorField = $('[for=' + $field.filter(':first').attr('id') + ']');
                            scrollAndFocus();
                        }
                        // Apply error appearance
                        $field.addClass( $field.hasClass('radio__input') ? 'radio__input_error' : 'checkbox__input_error' );
                    }
                    return false;
                }
                // Remove error appearance
                $field.removeClass( $field.hasClass('radio__input') ? 'radio__input_error' : 'checkbox__input_error' );
                return true;

            } else {    // If this is a text or select field.

                // if field is empty or "0" and should not be ignored:
                if ( ($.trim($field.val()).length === 0 || $.trim($field.val()) === "0" ) && !ignore) {
                    if (submitAttempted) {              // Error is showed if 'submit' button has already been clicked.
                        $field.addClass('form-row__field_error');
                        if (!$firstErrorField) {                                // if it's the first error field
                            $firstErrorField = $field;
                            scrollAndFocus();
                        }
                    }
                    return false;
                }
                $field.removeClass('form-row__field_error');
                return true;

            }
        }
    }

    // Checks if select option is not default and not unknown.
    function validateSelectIsFilledAndKnown(fieldName, ignore) {
        return function () {
            var $field = $('[name="' + fieldName + '"]');

            // if field is empty or "0" or "1" and should not be ignored:
            if ( ($.trim($field.val()).length === 0 || $.trim($field.val()) === "0" || $.trim($field.val()) === "1" ) && !ignore) {
                if (submitAttempted) {              // Error is showed if 'submit' button has already been clicked.
                    $field.addClass('form-row__field_error');
                    if (!$firstErrorField) {                            // if it's the first error field
                        $firstErrorField = $field;
                        scrollAndFocus();
                    }
                }
                return false;
            } else {
                $field.removeClass('form-row__field_error');
                return true;
            }
        }
    }

    // Checks if checkbox is not checked.
    function validateChbEmpty(fieldName, ignore) {
        return function() {
            var $field = $('[name="' + fieldName + '"]');
            // if checkbox is checked and should not be ignored:
            if ($('[name=' + fieldName + ']:checked').length !== 0 && !ignore) {
                if (submitAttempted) {              // Error is showed if 'submit' button has already been clicked.
                    if (!$firstErrorField) {                                    // if it's the first error in form
                        // Point $firstErrorField to label of this element
                        // (because radiobutton/checkbox is hidden and can't be scrolled to):
                        $firstErrorField = $('[for=' + $field.filter(':first').attr('id') + ']');
                        scrollAndFocus();
                    }
                    // Apply error appearance
                    $field.addClass( 'checkbox__input_error' );
                }
                return false;
            }
            // Remove error appearance
            $field.removeClass( 'checkbox__input_error' );
            return true;
        }
    }

    // Checks if this separate radiobutton is checked.
    function validateRbChecked(rbId) {
        return function () {
            var $rb = $('#'+rbId);
            if ( $('#'+rbId+':checked').length === 0 ) {    // Radiobutton is not checked.
                if (submitAttempted) {              // Error is showed if 'submit' button has already been clicked.
                    if (!$firstErrorField) {                                // if it's the first error in form
                        // Point $firstErrorField to label of this element
                        // (because radiobutton/checkbox is hidden and can't be scrolled to):
                        $firstErrorField = $('[for=' + rbId + ']');
                        scrollAndFocus();
                    }
                    // Apply error appearance to all rb in group
                    $('[name=' + $rb.attr('name') + ']').addClass('radio__input_error');
                }
                return false;
            }
            // Remove error appearance from all rb in group
            $('[name=' + $rb.attr('name') + ']').removeClass('radio__input_error');
            return true;
        }
    }

    //====== LOWEST-LEVEL VALIDATION FUNCTIONS =========================================================================

    // Show/hide block with error message
    function displayDiv(divId){
        var $div = $('#' + divId);
        return function(hideDiv) {
            if (submitAttempted) {      // Error is showed if 'submit' button has already been clicked.
                if (hideDiv) {
                    $div.hide(submitValidation ? 0 : "fast");
                } else {
                    $div.show(submitValidation ? 0 : "fast");
                }
            }
        }
    }

    // Show color/black-white icon in section header
    function changeHeaderIcon(section) {
        var $header = $('#' + section + '_header');
        return function (sectionValid) {
            if (sectionValid) {
                $header.removeClass('fsect__header_' + section + '-error');
                $header.addClass(   'fsect__header_' + section + '-valid');
            } else {
                $header.addClass(   'fsect__header_' + section + '-error');
                $header.removeClass('fsect__header_' + section + '-valid');
            }
        }
    }

    // Field validation
    function validate(validatorsArr) {
        return function() {
            var allValid = true;       // Empty array 'validatorsArr' will be valid.
            validatorsArr.forEach(function (validator) {
                var checkField = validator[0];  // Validation function
                var showError  = validator[1];  // Appearance function

                var fieldValid = checkField();
                if ( ! fieldValid ) {
                    allValid = fieldValid;
                }
                if ( showError ) {
                    showError(fieldValid);
                }
            });
            return allValid;
        }
    }


    //====== AUTO-FILLING FOR TESTING ==================================================================================
    if (typeof orderTesting == 'object' && orderTesting.enabled) {
        (function () {
            function fillWife() {
                $('#wife_fname').val('Wife-fname');
                $('#wife_lname').val('Wife-lname');

                $('#wife_year').val('1981');
                $('#wife_month').val('1');
                $('#wife_day').val('1');
                $('#wife_ssn2').val('1111');

                $('#wife_wants_divorce').val('yes');

                $('#wife_street_address').val('Wife-str-address');
                $('#wife_postal_address1').val('111 11');
                $('#wife_postal_address2').val('Wife-city');

                $('#wife_country').val('SWE');
                $('#wife_city').parents('.form-row__field-wrapper').show();
                $('#wife_city').val('karlshamn');

                var periodId = $('.period-wife').last().data("period-id") + 1;
                var periodInstanceText = $('#period-wife-template').text().replace(/\{\{N\}\}/g, periodId);
                $('.periods-wife').append(periodInstanceText);

                // Selects filling
                optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_country'), countries_list);
                optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_year'), years_list);
                optionsFill($('.periods-wife').find('.period-wife').last().find('.form-row__field_month'), months_list);

                $('#wife_living_country_0').val('ALG');
                $('#wife_living_from_0_year').val('1990');
                $('#wife_living_from_0_month').val('1');
                $('#wife_living_to_0_year').val('1991');
                $('#wife_living_to_0_month').val('2');
                $('#wife_living_country_1').val('US_');
                $('#wife_living_from_1_year').val('1992');
                $('#wife_living_from_1_month').val('3');
                $('#wife_living_to_1_year').val('1993');
                $('#wife_living_to_1_month').val('4');

                // $('#wife_country_citizen').val('1');
                $('#wife_country_citizen').val(['SWE', 'AFG', 'ALB']);

                dynamicValidate('wife_wants_divorce');
            }
            function fillHusband(sameAddress) {
                sameAddress = (sameAddress === undefined) ? true : sameAddress;

                $('#husband_fname').val('Husband-fname');
                $('#husband_lname').val('Husband-lname');

                $('#husband_year').val('1982');
                $('#husband_month').val('2');
                $('#husband_day').val('2');
                $('#husband_ssn2').val('2222');

                $('#husband_wants_divorce').val('yes');

                if (sameAddress) {
                    $('#husband_rb_y').prop('checked', true);
                    $('#husband_street_address').val('Wife-str-address');
                    $('#husband_postal_address1').val('111 11');
                    $('#husband_postal_address2').val('Wife-city');
                } else {
                    $('#husband_rb_n').prop('checked', true);
                    $('#husband-section').find('*').removeAttr('disabled');
                    $('#husband_street_address').val('Husband-str-address');
                    $('#husband_postal_address1').val('222 22');
                    $('#husband_postal_address2').val('Husband-city');
                }


                $('#husband_country').val('SWE');
                $('#husband_city').parents('.form-row__field-wrapper').show();
                $('#husband_city').val('ronneby');

                var periodId = $('.period-husband').last().data("period-id") + 1;
                var periodInstanceText = $('#period-husband-template').text().replace(/\{\{N\}\}/g, periodId);
                $('.periods-husband').append(periodInstanceText);

                // Selects filling
                optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_country'), countries_list);
                optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_year'), years_list);
                optionsFill($('.periods-husband').find('.period-husband').last().find('.form-row__field_month'), months_list);

                $('#husband_living_country_0').val('ALG');
                $('#husband_living_from_0_year').val('1990');
                $('#husband_living_from_0_month').val('1');
                $('#husband_living_to_0_year').val('1991');
                $('#husband_living_to_0_month').val('2');
                $('#husband_living_country_1').val('US_');
                $('#husband_living_from_1_year').val('1992');
                $('#husband_living_from_1_month').val('3');
                $('#husband_living_to_1_year').val('1993');
                $('#husband_living_to_1_month').val('4');

                $('#husband_country_citizen').val('SWE');

                dynamicValidate('husband_wants_divorce');
            }
            function fillChildren(number) {
                number = (number === undefined) ? 0 : number;

                if (number == 0) {
                    $('#children_rb_n').prop('checked', true);
                    $('#common-children-section, #custody-section, #living-section, .add-child').hide();
                } else {
                    $('#children_rb_y').prop('checked', true);
                    $('#common-children-section, #custody-section, #living-section').find('*').removeAttr('disabled');
                    $('#common-children-section, #custody-section, #living-section, .add-child').show();
                    for (var ind = 1; ind <= number; ind++) {

                        if (ind > 1) {
                            var childId = $('.child').last().data("child-id") + 1;
                            var childInstanceText = $('#child-template').text().replace(/\{\{N\}\}/g, childId);
                            $('.children').append(childInstanceText);

                            // Selects filling
                            optionsFill($('.children').find('.child').last().find('.form-row__field_year'), years_list);
                            optionsFill($('.children').find('.child').last().find('.form-row__field_month'), months_list);
                            optionsFill($('.children').find('.child').last().find('.form-row__field_day'), days_list);
                        }

                        $('#child_' + (ind-1) + '_fname').val('Child-' + ind + '-fname');
                        $('#child_' + (ind-1) + '_lname').val('Child-' + ind + '-lname');
                        $('#child_' + (ind-1) + '_year').val(2000 + ind);
                        $('#child_' + (ind-1) + '_month').val(ind);
                        $('#child_' + (ind-1) + '_day').val(ind);
                        $('#child_' + (ind-1) + '_ssn2').val(ind.toString().repeat(4));

                    }
                    $('.children .child').each( function(index) {
                        var $child = $(this);
                        var childId = $child.data("child-id");
                        var childFname = $child.find('.form-row__field_child-fname').val();
                        var childLname = $child.find('.form-row__field_child-lname').val();

                        var radioCustodyWifeStr = $('#child-radio-custody-template-wife').text().replace(/\{\{N\}\}/g, childId);
                        var $radioCustodyWife = $($.parseHTML(radioCustodyWifeStr));
                        $radioCustodyWife.find('.radio__label').text(childFname + ' ' + childLname);
                        $('#custody-section').find('.form-row_indented_wife').append($radioCustodyWife);

                        var radioCustodyHusbandStr = $('#child-radio-custody-template-husband').text().replace(/\{\{N\}\}/g, childId);
                        var $radioCustodyHusband = $($.parseHTML(radioCustodyHusbandStr));
                        $radioCustodyHusband.find('.radio__label').text(childFname + ' ' + childLname);
                        $('#custody-section').find('.form-row_indented_husband').append($radioCustodyHusband);

                        var radioLivingWifeStr = $('#child-radio-living-template-wife').text().replace(/\{\{N\}\}/g, childId);
                        var $radioLivingWife = $($.parseHTML(radioLivingWifeStr));
                        $radioLivingWife.find('.radio__label').text(childFname + ' ' + childLname);
                        $('#living-section').find('.form-row_indented_wife').append($radioLivingWife);

                        var radioLivingHusbandStr = $('#child-radio-living-template-husband').text().replace(/\{\{N\}\}/g, childId);
                        var $radioLivingHusband = $($.parseHTML(radioLivingHusbandStr));
                        $radioLivingHusband.find('.radio__label').text(childFname + ' ' + childLname);
                        $('#living-section').find('.form-row_indented_husband').append($radioLivingHusband);

                        $('#custody-divided').prop('checked', true);
                        $('#living-divided').prop('checked', true);
                        if (index%2 == 0) {
                            $('#child_' + index + '_custody-wife').prop('checked', true);
                            $('#child_' + index + '_living-wife').prop('checked', true);
                        } else {
                            $('#child_' + index + '_custody-husband').prop('checked', true);
                            $('#child_' + index + '_living-husband').prop('checked', true);
                        }
                    });
                }
                dynamicValidate('children_rb_y');
                dynamicValidate('child_0_fname');
            }
            function fillWaiting() {
                $('#think-yes').prop('checked', true);
                $('#living_together-yes').prop('checked', true);
                $('#childs_bellow_16-yes').prop('checked', true);

                dynamicValidate('think-yes');
            }
            function fillMarriage() {
                $('#marriage_country').val('SWE');
                $('#registered_in_sweden').val('yes');

                dynamicValidate('marriage_country');
            }
            function fillConfirm() {
                $('#payment').val('wife');
                $('#cellphone').val('070-123 45 67');
                $('#email').val('mail@example.se');
                $('#confirm').prop('checked', true);
            }
            function fill() {
                fillWife();
                fillHusband(false);
                fillChildren(2);
                fillWaiting();
                fillMarriage();
                fillConfirm();
            }

            fill();
        })();
    }
});